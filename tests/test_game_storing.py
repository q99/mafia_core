import os
import random
from datetime import datetime, timedelta
from pathlib import Path

from shutil import copyfile, move

from mafia_manager import Storage


def test_store_game():
    path = Path.cwd() / "data.db"
    _copy = Path.home() / ".cache" / "tmp"
    try:
        copyfile(str(path), str(_copy))

        storage = Storage(path)
        dt, uid = datetime.now(), storage.store_game(f"fake{random.randint(0,100000000)}")
        storage.store_action(uid, "Fake1", target=1, initiator=2)
        storage.store_action(uid, "Fake2", target=2, initiator=1)
        storage.store_action(uid, "Fake3", target=(1,2), initiator=(3,4))

        game = storage.load_game(uid=uid)
        assert (game.dt - dt) <= timedelta(seconds=1)
        assert len(game.history) == 3

        fake1, fake2, fake3 = game.history[0], game.history[1], game.history[2]

        assert fake1.action == "Fake1"
        assert fake2.action == "Fake2"

        assert fake1.initiator == [2]
        assert fake1.target == [1]

        assert fake2.initiator == [1]
        assert fake2.target == [2]

        assert fake3.action == "Fake3"
        assert fake3.initiator == [3,4]
        assert fake3.target == [1,2]

    finally:
        os.remove(str(path))
        move(str(_copy), str(path))
