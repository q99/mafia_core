#!/bin/python3.7

import random
from datetime import datetime, timedelta
from typing import Dict, Optional, Tuple

from mafia_core.card import Card
from mafia_core.game.game import Game
from mafia_core.player import Player, Property, Role
from mafia_core.track.actions import (Action, CommissarReview, Death,
                                      MafiaAcquaintance, NightVote)

from mafia_core.track.standard import Standard

info_list = list()

def create_users(count: int) -> Tuple[Player]:
    return tuple(Player(number,
                        lambda number, info: info_list.append((number, info)),
                        uid=random.randint(0, 100000))
                     for number in range(1, count+1))

_actions = list()
_action_registry = dict()

def registrar_local(action: str, initiator: Optional[Player], target: Optional[Player]):
    if isinstance(initiator, Player):
        initiator = initiator.number
    elif isinstance(initiator, tuple):
        initiator = tuple(map(lambda pl: pl.number, initiator))

    if isinstance(target, Player):
        target = target.number
    elif isinstance(target, tuple):
        target = tuple(map(lambda player: player.number, target))

    _actions.append((action, initiator, target))

def registrar_global(action: str, initiator: Optional[Player], target: Optional[Player]):
    if isinstance(initiator, Player):
        initiator = initiator.uid
    elif isinstance(initiator, tuple):
        initiator = tuple(pl.uid for pl in initiator)

    if isinstance(target, Player): target = target.uid
    elif isinstance(target, tuple): target = tuple(pl.uid for pl in target)

    action_dict = dict(action=action)
    if initiator:
        action_dict['initiator'] = initiator
    if target:
        action_dict['target'] = target

    _action_registry[str(datetime.now())] = action_dict

def registrar(action: str, initiator: Optional[Player], target: Optional[Player]):
    registrar_local(action, initiator, target)
    registrar_global(action, initiator, target)
    
def test_undercover():
    _actions.clear(), info_list.clear()
    count = 10
    game = Game(Standard(count), create_users(count))
    from mafia_core.track.cards import Undercover
    action: Undercover = game.additional_actions('Undercover')
    assert isinstance(action, Undercover)
    initiator_candidate = game.filter_alive(action.is_initiator)
    action.process(initiator_candidate[0], None, registrar=registrar)
    pass
    
def test_simple_standard_game():
    _actions.clear(), info_list.clear()

    count = 10
    game = Game(Standard(count), create_users(count))

    action = game.current_action()
    assert action.title == "Wait"
    additional = game.additional_actions()
    assert len(additional) == 1
    assert "Undercover" in additional

    game.next_action()

    additional = game.additional_actions()

    assert len(additional) == 3
    assert "ChangeRole" in additional.keys()
    change_role = additional["ChangeRole"]
    change_role.process(game[1], game[2], registrar)
    assert _actions.pop() == ("ChangeRole", 1, 2)
    assert "ChangeRole" not in game.additional_actions()

    action = game.current_action()

    assert isinstance(action, MafiaAcquaintance)
    assert all(player.role is Role.Civilian for player in game.alive)

    candidates = game.filter_alive(action.is_target)
    assert len(candidates) == len(game.alive)

    mafia: Tuple[int] = (1, 2, 3)
    action.process(None, game.get_players(*mafia), registrar)
    assert all(player.role is Role.Mafia
                   for player in game.alive if player.number in mafia)

    assert _actions.pop() == ("AcquaintanceMafia", None, mafia)

    action = game.next_action()

    candidates = game.filter_alive(action.is_target)
    assert len(candidates) == len(game.alive)

    target = random.choice(candidates)
    action.process(game[mafia], target, registrar)
    assert target[Property.is_victim]
    assert _actions.pop() == ("NightVote", mafia, target.number)

    assert "Recruitment" in game.additional_actions()
    recruitment = game.additional_actions()["Recruitment"]
    target = random.choice(tuple(filter(lambda pl: pl.role is not Role.Mafia, game.filter_alive(
        recruitment.is_target))))

    assert target.role is not Role.Mafia
    recruitment.process(game[mafia[2]], target, registrar)
    assert target.role is Role.Mafia
    assert _actions.pop() == ("Recruitment", mafia[2], target.number)
    assert "Recruitment" not in game.additional_actions()
    assert game[mafia[2]].card == Card.Recruitment

    action = game.next_action()
    assert action.title == "AcquaintanceCommissar"
    candidates = game.filter_alive(action.is_target)
    assert all(pl.role is not Role.Mafia for pl in candidates)
    target = random.choice(candidates)

    action.process(None, target, registrar)
    assert target.role is Role.Commissar

    action = game.next_action()
    assert action.title == "CommissarReview"
    candidates = game.filter_alive(action.is_target)
    target = random.choice(candidates)
    action.process(game.commissar, target, registrar)
    assert info_list.pop() == (game.commissar.number, (target.number, target.role))

    assert 'Review' in game.additional_actions()
    review = game.additional_actions()['Review']
    initiator = random.choice(tuple(filter(lambda pl: pl.role is not Role.Commissar, game.players_with_close_card())))
    target = random.choice(game.alive)
    review.process(initiator, target, registrar)

    assert initiator.card == Card.Review
    assert info_list.pop() == (initiator.number, (target.number, target.role))

    action = game.next_action()
    assert action.title == "Death"
    assert len(game.additional_actions()) == 2
    assert "Armor" in game.additional_actions()
    assert "Treatment" in game.additional_actions()

    initiator: Player = random.choice(game.players_with_close_card(roles=(Role.Civilian, Role.Mafia)))
    victim = game.victim
    game.additional_actions()['Treatment'].process(initiator, game.victim, registrar)
    assert initiator.card is Card.Treatment
    assert action.is_pseudo(game.alive)
    assert _actions.pop() == ('Treatment', initiator.number, victim.number)

    action = game.next_action()

    assert action.title == "Speak"
    assert len(game.filter_alive(action.is_target)) == len(game.alive)
    assert len(game.filter_alive(action.is_initiator)) == len(game.alive)
    assert action.get_process_time() == timedelta(minutes=1)
    action.process(game[1], None, registrar)

    assert len(game.filter_alive(action.is_initiator)) == len(game.alive) - 1
    assert len(game.filter_alive(action.is_target)) == len(game.alive)

    assert _actions.pop() == ("Speak", 1, None)
    assert not game[1][Property.is_speak]

    exposed = game[1], game[8]

    action = game.next_action()
    assert action.title == "Speak"
    action.process(game[2], exposed[0], registrar)

    assert len(game.filter_alive(action.is_initiator)) == len(game.alive) - 2
    assert len(game.filter_alive(action.is_target)) == len(game.alive) - 1

    assert _actions.pop() == ("Speak", 2, 1)
    assert game[1][Property.is_exposed]

    action = game.next_action()
    assert action.title == "Speak"
    action.process(game[3], exposed[1], registrar)

    assert _actions.pop() == ("Speak", 3, 8)

    for player in game.filter_alive(lambda player: player[Property.is_speak]):
        action = game.next_action()
        action.process(player, None, registrar)
        assert _actions.pop() == ("Speak", player.number, None)

    assert len(game.exposed) == len(exposed)

    action = game.next_action()
    assert action.title == "DayVote"
    exposed_player = game.next_exposed()
    assert exposed_player == exposed[0]
    action.process(5, exposed_player, registrar)

    assert _actions.pop() == ("DayVote", 5, exposed[0].number)
    assert exposed_player.votes == 5

    action = game.next_action()
    assert action.title == "DayVote"
    exposed_player = game.next_exposed()
    assert exposed_player == exposed[1]
    action.process(5, exposed_player, registrar)

    assert _actions.pop() == ("DayVote", 5, exposed[1].number)
    assert exposed_player.votes == 5

    death_action: Action = game.next_action()
    assert death_action.title == "Death"
    candidates = game.filter_alive(death_action.is_target)
    assert len(candidates) == 2

    additional: Dict[str, Action] = game.additional_actions()
    assert len(additional) == 1
    assert len(additional) == 1
    assert 'Leader' in additional
    leader_action: Action = additional['Leader']
    assert leader_action.title == "Leader"

    assert game.filter_alive(leader_action.is_initiator) == game.players_with_close_card()
    candidates = game.filter_alive(leader_action.is_target)
    assert candidates == game.filter_alive(lambda player: player[Property.is_victim])

    leader: Player = random.choice(game.players_with_close_card())
    leader_action.process(leader, candidates[0], registrar)

    assert _actions.pop() == (leader_action.title, leader.number, candidates[0].number)

    candidates = game.filter_alive(death_action.is_target)
    assert len(candidates) == 1
    assert death_action.is_initiator_optional

    death_action.process(None, candidates[0], registrar)

    assert _actions.pop() == (death_action.title, None, candidates[0].number)
    assert not candidates[0][Property.is_alive]
    assert len(game.alive) == 9

    action = game.next_action()
    assert action.is_constant_initiator()
    mafia: Tuple[Player] = game.filter_alive(action.is_initiator)
    assert set(mafia) == set(game.mafia)

    candidates = game.filter_alive(lambda pl: action.is_target(pl) and pl.role in (Role.Civilian, Role.Commissar))
    assert len(candidates) == (len(game.alive) - len(game.mafia))
    target = random.choice(candidates)
    action.process(mafia, target, registrar)

    assert _actions.pop() == ("NightVote", tuple(map(lambda pl: pl.number, mafia)), target.number)
    assert target[Property.is_victim]

    action: Action = game.next_action()

    is_pseudo = game.commissar is None
    if not is_pseudo:
        assert isinstance(action, CommissarReview)
        action.process(game.commissar, mafia[0], registrar)
        assert _actions.pop() == ('CommissarReview', game.commissar.number, mafia[0].number)
        assert info_list.pop() == (game.commissar.number, (mafia[0].number, Role.Mafia))
        action: Action = game.next_action()

    assert isinstance(action, Death)
    additional = game.additional_actions()
    assert len(additional) == 1
    assert 'Armor' in additional
    assert game.victim == target

    action.process(None, game.victim, registrar)

    assert _actions.pop() == ("Death", None, target.number)

    for player in game.alive:
        action = game.next_action()
        assert action.is_initiator(player)
        action.process(player, None, registrar)
        assert _actions.pop() == ("Speak", player.number, None)

    action = game.next_action()
    assert isinstance(action, NightVote)


if __name__ == "__main__":
    test_simple_standard_game()
