import random
from pathlib import Path

from mafia_manager import Evening, Member, Storage


def test_manager():
    strg = Storage(Path.cwd() / "data.db")
    evening = Evening(master=Member.load(strg, name="Михаил Сергеевич"), storage=strg)
    tuple(evening.add_member(member) for member in evening.get_potential_member())
    game = evening.start_game(channel=lambda number, info: print(number, info))
