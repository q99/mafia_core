import logging
from dataclasses import dataclass, field
from random import choices, shuffle
from typing import List, Union, Tuple, Type, Optional, Dict, Callable, Any
from uuid import UUID

from mafia_core.game.game import Game
from mafia_manager.member import Member
from mafia_manager.storage import Storage
from mafia_core.track.standard import Standard
from mafia_core.track.track import Track


@dataclass
class Evening:
    master: Union[Member, str, int]
    storage: Storage
    members: List[Member] = field(default_factory=list)
    game: Optional[Game] = None

    
    def __post_init__(self):
        if isinstance(self.master, str):
            master = self.master
            self.master = Member.load(storage=self.storage, name=self.master)
            
        elif isinstance(self.master, int):
            master = self.master
            self.master = Member.load(storage=self.storage, uid=self.master)
            
        elif not isinstance(self.master, Member):
            raise ValueError(f"Can't initialize master from {self.master} ({type(self.master)})")
        
        else:
            return
        
        assert isinstance(self.master, Member), f"Can't load master {master} from {self.storage}"
    
    def start_game(self,
                   players: Union[int, Tuple[Member]]=None,
                   track: Type[Track] = Standard,
                   channel: Callable[[int, Any], None]=None) -> Game:
    
        if isinstance(players, int) or players is None:
            count = players or track.max_count()
            players = list(self.members)
            shuffle(players)
            players = players[:count]
        elif isinstance(players, list):
            assert all(player in self.members for player in players)
            
            
        players = tuple(player.uid for player in players)
        self.game = Game(track(len(players)), _players=players, channel=channel)
        self.game.uid = self.storage.store_game()
        return self.game
    
    def get_name(self, number: int) -> Optional[str]:
        for player in self.members:
            if player.uid == self.game[number].uid:
                return str(player)
        return None
    
    def get_numbers(self) -> Dict[int, Member]:
        players: Dict[int, int] = {player.uid: player.number for player in self.game.alive}
        numbers = {players[member.uid]: member for member in self.members if member.uid in players}.items()
        return dict(sorted(numbers))
     
    def get_potential_member(self) -> Tuple[Member]:
        members = Member.load_several(self.storage)
        return tuple(filter(lambda member: member not in self.members and member != self.master, members))
    
    def add_member(self, member: Union[str, int, Member]):
        if isinstance(member, int):
            member = Member.load(uid=member, storage=self.storage)
        elif isinstance(member, str):
            member = Member.load(name=member, storage=self.storage)
            
        self.members.append(member)
    
    def remove_member(self, member: Union[int, Member]):
        if isinstance(member, int):
            def filter_(member_: Member):
                return member_.uid == member
        elif isinstance(member, Member):
            def filter_(member_: Member):
                return member_ == member
        else:
            raise ValueError()
        
        self.members = list(player for player in self.members if not filter_(player))
        
    def clean(self):
        self.members.clear()

    def is_ready(self, track: Type[Track] = Standard):
        return len(self.members) > track.min_count()