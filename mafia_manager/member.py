from dataclasses import dataclass
from typing import Optional, Iterable, Tuple

from mafia_manager.storage import Storage


@dataclass
class Member:
    name: str
    storage: Storage
    _uid: Optional[int] = None
    
    def __post_init__(self):
        pass

    def __eq__(self, member: 'Member') -> bool:
        return self.name == member.name and self._uid == member.uid

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.__repr__()

    def __hash__(self):
        return self._uid.__hash__()
    
    @property
    def uid(self) -> int:
        assert self._uid is not None, f"Can't get uid. {self.name} did't store in db."
        return int(self._uid)
    
    @property
    def is_stored(self) -> bool:
        return self._uid is not None
    
    @classmethod
    def load(cls, storage: Storage, *, name: Optional[str] = None, uid: Optional[int] = None) -> 'Member':
        if name:
            assert isinstance(name, str)
            member = storage.load_member(name)
        elif uid:
            assert isinstance(uid, int)
            member = storage.load_member(uid)
        else:
            raise ValueError(f"Can't load member without arguments.")
    
        return Member(_uid=member[0], name=member[1], storage=storage)

    @classmethod
    def load_several(cls, storage: Storage, name: str = None, is_master: bool = None) -> Iterable['Member']:
        return (Member(_uid=member[0], name=member[1], storage=storage)
            for member in storage.load_members(name=name, is_master=is_master))
    
    def store(self):
        self._uid = self.storage.store_member(self.name)
        return self
       
