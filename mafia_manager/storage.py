from collections import namedtuple
from dataclasses import field, dataclass
from datetime import datetime
from pathlib import Path
from sqlite3 import connect
from typing import overload, Tuple, Union, Optional, Iterable, AnyStr, Dict, List

@dataclass
class _Action:
    action: str = None
    initiator: List[int] = field(default_factory=list)
    target: List[int] = field(default_factory=list)
    
@dataclass
class _Game:
    dt: datetime = None
    comment: str = None
    history: Dict[int, _Action] = field(default_factory=dict)

class open_cursor(object):
    """
    Simple CM for sqlite3 databases. Commits everything at exit.
    """
    def __init__(self, db: Path):
        assert db.exists()
        self.conn = connect(str(db))
        self.cursor = None

    def __enter__(self):
        self.cursor = self.conn.cursor()
        return self.cursor

    def __exit__(self, exc_class, exc, traceback):
        self.conn.commit()
        self.cursor.close()
        self.conn.close()

class Storage:
    def __init__(self, database: Path):
        if isinstance(database, str): database = Path(database)
        assert database.exists()
        self._database = database
        
    def __str__(self):
        return str(self._database)
    
    @overload
    def load_member(self, name: str) -> Tuple[int, str]:
        pass
    
    @overload
    def load_member(self, uid: int) -> Tuple[int, str]:
        pass
    
    def load_member(self, val: Union[str, int]) -> Tuple[int, str]:
        if isinstance(val, str):
            val = f"%{val}%"
            sql = "SELECT uid, name FROM members WHERE members.name LIKE ?"
        elif isinstance(val, int):
            val = str(val)
            sql = "SELECT uid, name FROM members WHERE members.uid = ?"
            
        with open_cursor(self._database) as cursor:
            cursor.execute(sql, (val,))
            member = cursor.fetchall()
        
        if len(member) != 1:
            raise ValueError(f"The response of the request is ambiguous. Refine the name or use `load_members` method")
        
        member = member[0]
        
        if member is None:
            raise KeyError(f"Can't find member with {val}")
        
        assert len(member) == 2
        assert isinstance(member[0], int)
        assert isinstance(member[1], str)
        
        return int(member[0]), member[1]
     
    def load_members(self, name: str = None, is_master: bool = None):
        sql = "SELECT uid, name FROM members"
        args = list()
        
        if any(arg is not None for arg in (name, is_master)):
            sql += " WHERE "
            args = dict()
            if name:
                args["name LIKE ?"] = f"%{name}%"
            if is_master:
                args["is_master = ?"] = is_master
            sql += "AND ".join(args.keys())
            args = list(args.values())
        
        with open_cursor(self._database) as cursor:
            cursor.execute(sql, args)
            
            while cursor:
                member = cursor.fetchone()
                if member is None:
                    return
                yield int(member[0]), member[1]
            
        
    def store_member(self, name: str) -> int:
        with open_cursor(self._database) as cursor:
            cursor.execute("INSERT INTO members (name) VALUES (@name_)", dict(name_=name))
            cursor.execute("SELECT uid FROM members WHERE name = @name_", dict(name_=name))
            uid = cursor.fetchone()
            assert len(uid) == 1
            return uid[0]
    
    def load_games(self) -> Iterable[Tuple[int, datetime, str]]:
        with open_cursor(self._database) as cursor:
            cursor.execute("SELECT games.uid, games.ts, games.comment FROM games")
            for raw in cursor:
                uid, ts, comment = raw
                yield uid, datetime.fromtimestamp(ts), comment
    
    
    def load_game(self, uid: int) -> _Game:
        with open_cursor(self._database) as cursor:
            cursor.execute("""SELECT games.ts,
                                     games.comment,
                                     history.move_number,
                                     history.action,
                                     history.player,
                                     history.is_target
                               FROM games, history
                               WHERE games.uid = @uid AND history.game = games.uid
                               ORDER BY history.move_number""", dict(uid=uid))
            
            game = _Game()
            for raw in cursor:
                ts, comment, move_number, action, player, is_target = raw
                
                if game.dt is None:
                    game.dt = datetime.fromisoformat(ts)
                if game.comment is None:
                    game.comment = comment
                
                game.history[move_number] = game.history.get(move_number, _Action())
                game.history[move_number].action = game.history[move_number].action or action
                if is_target:
                    game.history[move_number].target.append(player)
                else:
                    game.history[move_number].initiator.append(player)
                    
            return game
            
            
            
    def store_game(self, comment: Optional[str] = None) -> int:
        '''
        Store new game in database and return game id
        :param comment: Optional comment to game (location, club etc)
        :return: Uid of game from database
        '''
        with open_cursor(self._database) as cursor:
            if comment:
                cursor.execute("INSERT INTO games (comment) VALUES (@comment)", dict(comment=comment))
            else:
                cursor.execute("INSERT INTO games DEFAULT VALUES")
            cursor.execute("SELECT max(uid) from games")
            uid = cursor.fetchone()
            
            assert len(uid) == 1
            assert isinstance(uid[0], int)
            
            return uid[0]
    
    def store_action(self, game: int, action: str, target: Union[int, Tuple[int]], initiator: Union[int, Tuple[int]]):
        if isinstance(initiator, int):
            initiator = initiator,
        elif initiator is None:
            initiator = tuple()
        if isinstance(target, int):
            target = target,
        elif target is None:
            target = tuple()
            
        with open_cursor(self._database) as cursor:
            cursor.execute("SELECT max(move_number) FROM main.history WHERE game == @game", dict(game=game))
            val = cursor.fetchone()[0]
            if val is not None:
                move_number = val + 1
            else:
                move_number = 0
    
            for player in target:
                self._store_action(cursor, game, action, player, True, move_number)
            for player in initiator:
                self._store_action(cursor, game, action, player, False, move_number)
           
    def _store_action(self,
                      cursor,
                      game: int,
                      action: str,
                      player: Union[int,Tuple[int]],
                      is_target: bool,
                      move_number: int):
        cursor.execute(
            'INSERT INTO history (game, action, player, is_target, move_number) '
            'VALUES (@game, @action, @player, @is_target, @move_number)',
            dict(game=game, action=action, player=player, is_target=is_target, move_number=move_number))
    
        