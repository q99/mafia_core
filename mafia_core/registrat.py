from typing import Callable
from typing import Union, Tuple

from mafia_core.player import Player
Players = Union[Player, Tuple[Player]]

Registrar = Callable[[str, Players, Players], None]
Rollback = Registrar

def logger(action: str, initiator: Players, target: Players):
    import logging
    logging.info(f"{action} "
                 f"[{initiator.number}, {initiator.uid}] -> "
                 f"[{target.number}, {target.uid}]")
