from enum import Enum

class Card(Enum):
    ChangeRole = 0, False
    Treatment = 1, True
    Recruitment = 2, False
    Armor = 3, True
    Theft = 4, True
    Alibi = 5, True
    Neutral = 6, None
    Leader = 7, True
    Undercover = 8, True
    Review = 9, False

    @property
    def is_night(self) -> bool:
        return self.value[1] is False

    @property
    def title(self) -> str:
        return self.name

    @property
    def is_reusable(self):
        return self is Card.Leader
