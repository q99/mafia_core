from mafia_core.card import Card
from mafia_core.registrat import Rollback
from .action import CardAction
from mafia_core.player import Player, Role, Property


class Recruitment(CardAction):
    card = Card.Recruitment
    
    def is_target(self, player: Player) -> bool:
        return player[Property.is_alive] and not player[Property.is_night_protected]
    
    @property
    def available_roles(self):
        return {Role.Mafia,}
    
    
    @CardAction.preprocess
    def process(self, initiator: Player, target: Player, registrar):
        target.role = Role.Mafia
        
    @CardAction.rollback_preprocess
    def rollback(self, initiator: Player, target: Player, rollback: Rollback):
        target.role = Role.default()
