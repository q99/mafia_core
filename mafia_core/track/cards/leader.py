from typing import Optional, Tuple

from mafia_core.card import Card
from .action import CardAction
from mafia_core.player import Player, Property, Players
from mafia_core.registrat import Registrar, Rollback


class Leader(CardAction):
    card = Card.Leader

    def is_pseudo(self, players: Tuple[Player]) -> bool:
        victims = tuple(filter(lambda player: player[Property.is_victim], players))
        return len(victims) == 1

    def is_target(self, player: Player) -> bool:
        return player[Property.is_victim]

    @CardAction.preprocess
    def process(self, initiator: Players, target: Optional[Players], registrar: Registrar):
        target.votes -= 1
        target[Property.is_victim] = False

    @CardAction.rollback_preprocess
    def rollback(self, initiator: Players, target: Player, rollback: Rollback):
        target.votes += 1
        target[Property.is_victim] = True
