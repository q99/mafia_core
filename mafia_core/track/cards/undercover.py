from typing import Union, Optional

from .action import CardAction

from mafia_core.card import Card
from mafia_core.player import Property, Player
from mafia_core.registrat import Registrar, Rollback
from mafia_core.player import Players

class Undercover(CardAction):
    card = Card.Undercover

    def is_target_optional(self):
        return True

    @CardAction.preprocess
    def process(self, initiator: Players, target: None, registrar: Registrar):
        assert initiator == target or target is None
        initiator[Property.is_night_review_protected] = True
        initiator[Property.is_night_protected] = True
        initiator[Property.is_day_protected] = True

    @CardAction.rollback_preprocess
    def rollback(self, initiator: Union[Players, int], target: Optional[Players], rollback: Rollback):
        initiator[Property.is_night_review_protected] = False
        initiator[Property.is_night_protected] = False
        initiator[Property.is_day_protected] = False
    
