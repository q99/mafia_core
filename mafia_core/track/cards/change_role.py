from mafia_core.card import Card
from mafia_core.player import Player, Property
from mafia_core.registrat import Registrar, Rollback
from .action import CardAction


class ChangeRole(CardAction):
    card = Card.ChangeRole
    
    def is_target(self, player: Player):
        tmp = player[Property.is_night_review_protected]
        return not tmp
    
    @property
    def initiator_count(self) -> int:
        return 1
    
    @property
    def target_count(self) -> int:
        return 1
    
    @CardAction.preprocess
    def process(self, initiator: Player, target: Player, registrar: Registrar):
        
        assert isinstance(initiator, Player)
        assert isinstance(target, Player)
        
        initiator.role, target.role = target.role, initiator.role
        

    @CardAction.rollback_preprocess
    def rollback(self, initiator: Player, target: Player, rollback: Rollback):
        initiator.role, target.role = target.role, initiator.role
        