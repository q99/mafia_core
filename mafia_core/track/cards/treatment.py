from mafia_core.card import Card
from mafia_core.player import Player, Property
from mafia_core.registrat import Rollback
from .action import CardAction


class Treatment(CardAction):
    card: Card = Card.Treatment

    @property
    def initiator_count(self) -> int:
        return 1

    @property
    def target_count(self) -> int:
        return 1

    @CardAction.preprocess
    def process(self, initiator: Player, target: Player, registrar):
        
        assert target[Property.is_alive]
        assert target[Property.is_victim]
        
        target[Property.is_victim] = False
        
    @CardAction.rollback_preprocess
    def rollback(self, initiator: Player, target: Player, rollback: Rollback):
        target[Property.is_victim] = True
