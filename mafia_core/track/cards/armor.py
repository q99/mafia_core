from typing import Optional, Set

from mafia_core.card import Card
from mafia_core.player import Player, Property, Role
from mafia_core.registrat import Players, Rollback
from mafia_core.track.cards import CardAction


class Armor(CardAction):
    card = Card.Armor

    def is_target(self, player: Player):
        return player[Property.is_victim]

    @property
    def available_roles(self) -> Set[Role]:
        return {Role.Civilian, Role.Mafia}

    @CardAction.preprocess
    def process(self, initiator: Players, target: Optional[Players], registrar):

        assert initiator == target
        assert initiator[Property.is_victim]
        
        initiator[Property.is_victim] = False
        initiator.card = Card.Armor
    
    @CardAction.rollback_preprocess
    def rollback(self, initiator: Player, target: Player, rollback: Rollback):
        assert initiator == target
        assert not initiator[Property.is_victim]
        
        initiator[Property.is_victim] = True
        initiator.card = None

