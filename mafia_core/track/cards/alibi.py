from typing import Optional

from mafia_core.card import Card
from mafia_core.player import Property
from mafia_core.registrat import Registrar, Players, Rollback
from mafia_core.track.cards import CardAction


class Alibi(CardAction):
    card = Card.Alibi
    
    def __init__(self):
        self._is_exposed = None
        
    @property
    def target_count(self) -> int:
        return 1
    
    @CardAction.preprocess
    def process(self, initiator: Players, target: Optional[Players], registrar: Registrar):
        self._is_exposed = target[Property.is_exposed]
        
        target[Property.is_exposed] = False
        target[Property.is_day_protected] = True
        
    @CardAction.rollback_preprocess
    def rollback(self, initiator: Players, target: Players, rollback: Rollback):
        target[Property.is_exposed] = self._is_exposed
        target[Property.is_day_protected] = False
