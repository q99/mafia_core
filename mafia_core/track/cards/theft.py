from typing import Optional

from mafia_core.card import Card
from mafia_core.registrat import Rollback
from .action import CardAction
from mafia_core.player import Player, Players


class Theft(CardAction):
    card = Card.Theft

    def is_target(self, player: Player):
        return player.card is None

    @property
    def target_count(self) -> int:
        return 1

    @CardAction.preprocess
    def process(self, initiator: Player, target: Player, registrar):
        initiator.card, target.card = target.card, initiator.card
    
    @CardAction.rollback_preprocess
    def rollback(self, initiator: Player, target: Player, rollback: Rollback):
        initiator.card, target.card = target.card, initiator.card
