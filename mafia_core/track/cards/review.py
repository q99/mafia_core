from typing import Optional

from mafia_core.card import Card
from .action import CardAction
from mafia_core.player import Player, Players, Property, Role


class Review(CardAction):
    card = Card.Review
    
    @property
    def available_roles(self):
        return {Role.Mafia, Role.Civilian}
    
    
    def is_target(self, player: Player) -> bool:
        return not player[Property.is_night_review_protected]
    
    @property
    def target_count(self) -> int:
        return 1
    
    @CardAction.preprocess
    def process(self, initiator: Players, target: Optional[Players], registrar):
        initiator.accept_information((target.number, target.role))
    
