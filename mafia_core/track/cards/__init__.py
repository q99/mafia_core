from typing import Dict

from mafia_core.card import Card

from .action import CardAction

from .alibi import Alibi
from .armor import Armor
from .change_role import ChangeRole
from .recruitment import Recruitment
from .review import Review
from .theft import Theft
from .treatment import Treatment
from .leader import Leader
from .undercover import Undercover

actions: Dict[Card, 'Action'] = \
    {
        Card.Alibi: Alibi,
        Card.Armor: Armor,
        Card.ChangeRole: ChangeRole,
        Card.Recruitment: Recruitment,
        Card.Review: Review,
        Card.Theft: Theft,
        Card.Treatment: Treatment,
        Card.Leader: Leader,
        Card.Undercover: Undercover
    }