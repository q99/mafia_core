from abc import abstractmethod
from typing import Callable, Union, Tuple

from mafia_core.registrat import Registrar, Players, Rollback
from mafia_core.track.actions import Action
from mafia_core.card import Card
from mafia_core.player import Player, Role


class CardAction(Action):
    card: Card = None
    
    def is_initiator(self, player: Player) -> bool:
        return player.role in self.available_roles and (player.card is None or player.card.is_reusable)

    def is_pseudo(self, players: Tuple[Player]) -> bool:
        return self.card in set(player.card for player in players if player.card and not player.card.is_reusable)

    @property
    def title(self):
        return self.card.title

    @property
    def initiator_count(self) -> int:
        return 1

    @property
    def target_count(self) -> int:
        return 1

    @staticmethod
    def preprocess(process: Callable):
        
        def wrapper(self: CardAction, initiator: Player, target: Union[Tuple[Player], Player], registrar: Registrar):
            if isinstance(initiator, tuple):
                assert len(initiator) == 1
                initiator = initiator[0]
            
            if isinstance(target, tuple):
                if len(target) == 1:
                    target = target[0]
                elif len(target) == 0:
                    target = None
            
            assert isinstance(initiator, Player)
            assert initiator.role in self.available_roles
            
            if self.target_count == 1:
                if self.is_target_optional:
                    assert isinstance(target, Player) or target is None or len(target) == 0
                else:
                    assert isinstance(target, Player)
            else:
                assert len(target) == self.target_count
            
            assert initiator.card in [None, self.card]
            
            initiator.card = self.card
            
            result = process(self, initiator, target, registrar)

            self.register(registrar, initiator, target)
            
            return result
        
        return wrapper

    @staticmethod
    def rollback_preprocess(function: Callable):
        
        def wrapper(self: Action, initiator: Players, target: Players, rollback: Rollback):
            result = super().rollback_preprocess(function)
            initiator.card = None
            rollback(self.title, initiator, target)
            return result
        
        return wrapper
    
    