from typing import Tuple, Set, Type, Union, Optional

from mafia_core.card import Card
from mafia_core.player import Role, Player

import mafia_core.track.states as states
from .states import State
from mafia_core.track.track import Track

_cards: Set[Card] = \
    {
        Card.Alibi,
        Card.Armor,
        Card.ChangeRole,
        Card.Leader,
        Card.Recruitment,
        Card.Review,
        Card.Theft,
        Card.Treatment,
    }


class Standard(Track):
    def __init__(self, count: int):
        assert count in (8,9,10), f"Can' start standard game with {count} players"
        self._count = count

    @staticmethod
    def max_count() -> int:
        return 10

    @staticmethod
    def min_count() -> int:
        return 8

    @classmethod
    def start(cls) -> State:
        return states.FirstDay(None, _cards)

    @classmethod
    def next(cls, state: State, players: Tuple[Player]=None, spent_cards: Tuple[Card] = None) -> State:

        if isinstance(state, states.FirstDay):
            return states.FirstNight(None, _cards)

        spent_cards = spent_cards or cls._spent_card(players)
        cards = _cards - spent_cards

        if players and state.is_lynching(players):
            cls.refresh(players, state)
            return states.Lynching(next_=type(cls.next(state, None, spent_cards)),
                                   players=players,
                                   available_cards=cards)

        if isinstance(state, states.Lynching):
            return state.next(players, cards)

        if isinstance(state, states.FirstNight) or isinstance(state, states.Night):
            cls.refresh(players, state)
            return states.Day(players, cards)

        if isinstance(state, states.Day):
            cls.refresh(players, state)
            return states.Night(players, cards)

    @classmethod
    def refresh(cls, players: Optional[Tuple[Player]], state: Union[State, Type[State]]):
        if players is None:
            return

        if isinstance(state, State):
            state = type(state)

        is_day_end = (state not in (states.FirstNight, states.Night))
        for player in players:
            player.refresh(is_day_end)

    def player_count(self, role: Role=None) -> int:
        if role is None:
            return self._count

        if role is Role.Mafia:
            if self._count in (10, 9):
                return 3
            if self._count == 8:
                return 2

        if role is Role.Commissar:
            return 1

        if role is Role.Civilian:
            if self._count == 10:
                return 6
            if self._count == 9:
                return 5
            if self._count == 8:
                return 4


    @staticmethod
    def _spent_card(players: Tuple[Player]) -> Set[Card]:
        return set(player.card for player in players
                if player.card is not None and not player.card.is_reusable)

    def is_win(self, players: Tuple[Player]) -> Optional[Role]:
        mafia = tuple(filter(lambda pl: pl.role is Role.Mafia, players))
        if len(mafia) == 0:
            return Role.Civilian

        is_treatment = any(player.card is Card.Treatment for player in players)
        is_armor = any(player.card is Card.Armor for player in players)

        if len(mafia) > (len(players) // 2):
            if is_armor and is_treatment:
                return Role.Mafia

        return None

