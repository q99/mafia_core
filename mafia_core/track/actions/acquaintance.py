from typing import Tuple, Union, Optional

from mafia_core.registrat import Registrar, Rollback
from mafia_core.track.actions import Action
from mafia_core.player import Role, Player
from mafia_core.track.actions.action import Players


class Acquaintance(Action):
    _Role: Role = None
    
    @property
    def available_roles(self):
        return {self._Role,}

    @property
    def is_initiator_optional(self):
        return True

    def is_target(self, player: Player):
        return player.role is Role.Civilian

    @property
    def title(self):
        return f"Acquaintance{self._Role.name}"

    @property
    def initiator_count(self) -> int:
        return 0

    @property
    def target_count(self) -> int:
        if self._Role == Role.Mafia:
            return 3
        if self._Role == Role.Commissar:
            return 1

    def process(self, initiator: None, target: Tuple[Player], registrar: Registrar):
        assert initiator is None or len(initiator) == 0, f"{initiator}"
        
        if isinstance(target, tuple) or isinstance(target, set):
            assert len(target) == self.target_count
            for player in target:
                player.role = self._Role
        else:
            assert isinstance(target, Player)
            target.role = self._Role
            
        
        self.register(registrar, initiator, target)
    
    @Action.rollback_preprocess
    def rollback(self, initiator: Union[Players, int], target: Optional[Players], rollback: Rollback):
        assert initiator is None
        
        if not isinstance(initiator, tuple) and isinstance(initiator, Player):
            initiator = initiator,
        
        for player in initiator:
            player.role = Role.default()


class MafiaAcquaintance(Acquaintance):
    _Role: Role = Role.Mafia


class CommissarAcquaintance(Acquaintance):
    _Role: Role = Role.Commissar

