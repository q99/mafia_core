from typing import Optional, Tuple

from mafia_core.player import Player
from mafia_core.registrat import Registrar
from .action import Action
from ...player import Players

class Wait(Action):
    
    @property
    def title(self):
        return "Wait"

    @property
    def target_count(self) -> int:
        return 0

    @property
    def initiator_count(self) -> int:
        return 0

    def is_pseudo(self, players: Tuple[Player]) -> bool:
        return True
