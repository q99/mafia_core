from typing import Optional, Union, Tuple

from mafia_core.player import Role, Property, Player
from mafia_core.registrat import Rollback
from mafia_core.track.actions import Action

Players = Union[Player, Tuple[Player]]

class NightVote(Action):

    def is_constant_initiator(self) -> bool:
        return True

    def is_initiator(self, player: Player) -> bool:
        return player.role is Role.Mafia

    def is_target(self, player: Player):
        return not player[Property.is_night_protected]

    @property
    def title(self):
        return "NightVote"

    @property
    def initiator_count(self) -> Tuple[int]:
        return (3, 4)

    @property
    def target_count(self) -> int:
        return 1

    def process(self, initiator: Players, target: Optional[Players], registrar):
        assert len(initiator) <= max(self.initiator_count), f"{len(initiator)}"

        assert all(player.role is Role.Mafia for player in initiator)

        if target:
            assert target[Property.is_alive]
            assert not target[Property.is_night_protected]
            target[Property.is_victim] = True

        self.register(registrar, initiator, target)

    @Action.rollback_preprocess
    def rollback(self, initiator: Players, target: Player, rollback: Rollback):
        if target:
            target[Property.is_victim] = False
