from typing import Tuple

from mafia_core.player import Player, Property, Role
from mafia_core.registrat import Registrar, Rollback
from mafia_core.track.actions import Action


class CommissarReview(Action):

    def is_constant_initiator(self) -> bool:
        return True

    def is_initiator(self, player: Player) -> bool:
        return player.role is Role.Commissar

    def is_target(self, player: Player) -> bool:
        return not player[Property.is_night_review_protected]

    @property
    def initiator_count(self) -> int:
        return 1

    @property
    def title(self):
        return "CommissarReview"

    @property
    def target_count(self) -> int:
        return 1

    @Action.preprocess
    def process(self, initiator: Player, target: Player, registrar: Registrar):
        assert not target[Property.is_night_review_protected]
        assert initiator.role is Role.Commissar

        initiator.accept_information((target.number, target.role))

    def is_pseudo(self, players: Tuple[Player]) -> bool:
        return not any(player.role is Role.Commissar for player in players)

    @Action.rollback_preprocess
    def rollback(self, initiator: Player, target: Player, rollback: Rollback):
        pass
