from abc import abstractmethod
from collections import namedtuple
from dataclasses import dataclass
from datetime import timedelta
from typing import Union, Callable, Optional, Tuple, Set

from mafia_core.player import Player, Role
from mafia_core.registrat import Registrar, Rollback

Players = Union[Player, int, Tuple[Player], None]

Info = namedtuple("Info", ("title", "target_choose", "initiator_choose", "success_end", "failed_end"))

@dataclass
class ActionMemory:
    initiator: Set[Player] = None
    targets: Set[Player] = None

class Action:
    def __init__(self):
        self.memory = ActionMemory()

    def approve_initiator_count(self, count: int):
        if isinstance(self.initiator_count, int):
            return count == self.initiator_count
        if isinstance(self.initiator_count, tuple):
            return count in self.initiator_count
        
        raise ValueError(f"initiator count have wrong type {type(self.initiator_count)}")
    
    def approve_target_count(self, count: int):
        assert isinstance(self.target_count, int), f"In {type(self)} target count is f{type(self.target_count)} and " \
            f"equal {self.target_count}"
        
        return self.target_count == count
    
    @property
    def available_roles(self) -> Set[Role]:
        return {Role.Mafia, Role.Commissar, Role.Civilian}

    def is_roles_contained(self, main: 'Action'):
        assert isinstance(main.available_roles, set)
        assert isinstance(self.available_roles, set)

        return any(role in main.available_roles for role in self.available_roles)

    def get_process_time(self) -> Optional[timedelta]:\
        return None

    def is_constant_initiator(self) -> bool:
        return False

    @property
    def is_simple_initiator_available(self) -> bool:
        return False

    @abstractmethod
    def is_initiator(self, player: Player) -> bool:
        pass

    @abstractmethod
    def is_target(self, player: Player) -> bool:
        pass

    @property
    @abstractmethod
    def title(self) -> str:
        pass

    def __str__(self):
        return self.title

    @property
    @abstractmethod
    def initiator_count(self) -> int:
        pass

    @property
    @abstractmethod
    def target_count(self) -> int:
        pass

    @abstractmethod
    def process(self, initiator: Union[Players, int], target: Optional[Players], registrar: Registrar):
        pass

    @abstractmethod
    def rollback(self, initiator: Union[Players, int], target: Optional[Players], rollback: Rollback):
        pass

    @abstractmethod
    def end_action(self, players: Players):
        pass

    @abstractmethod
    def register(self, registrar: Registrar, initiator: Players, target: Optional[Players]) -> str:
        registrar(self.title, initiator, target)

    @abstractmethod
    def is_pseudo(self, players: Tuple[Player]) -> bool:
        return False

    @property
    def is_target_optional(self):
        return self.target_count == 0

    @property
    def is_initiator_optional(self):
        return self.initiator_count == 0

    @staticmethod
    def rollback_preprocess(function: Callable):

        def wrapper(self: Action, initiator: Players, target: Players, rollback: Rollback):
            result = function(self, initiator, target)
            rollback(self.title, initiator, target)
            return result

        return wrapper

    @staticmethod
    def preprocess(process: Callable):
        def wrapper(self: Action, initiator: Players, target: Players, registrar: Registrar):

            if self.initiator_count == 1:
                assert isinstance(initiator, Player)
            elif self.initiator_count == 0:
                assert initiator is None
            else:
                assert len(initiator) == self.initiator_count

            if self.is_target_optional and target is None:
                pass
            elif self.target_count == 1:
                assert isinstance(target, Player)
            elif self.target_count == 0:
                assert target is None
            else:
                assert len(target) == self.target_count


            result = process(self, initiator, target, registrar)
            self.register(registrar, initiator, target)

            return result

        return wrapper

