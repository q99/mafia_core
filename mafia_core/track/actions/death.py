from typing import Optional, Tuple

from mafia_core.player import Player, Property
from mafia_core.registrat import Registrar, Rollback
from .action import Action
from .action import Players


class Death(Action):
    def is_target(self, player: Player):
        return player[Property.is_victim]

    @property
    def title(self):
        return "Death"

    @property
    def initiator_count(self) -> int:
        return 0

    @property
    def target_count(self) -> int:
        return 1
    
    @Action.preprocess
    def process(self, initiator: Players, target: Optional[Players], registrar: Registrar):
        assert target[Property.is_victim]
        target[Property.is_alive] = False

    def is_pseudo(self, players: Tuple[Player]) -> bool:
        return any(not player[Property.is_victim] for player in players)
    
    @Action.rollback_preprocess
    def rollback(self, initiator: Players, target: Player, rollback: Rollback):
        target[Property.is_alive] = True
