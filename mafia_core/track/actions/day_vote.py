from typing import Optional, Tuple, Union

from mafia_core.registrat import Registrar, Rollback
from mafia_core.track.actions import Action
from mafia_core.player import Property, Player, Players

class DayVote(Action):

    @property
    def title(self):
        return "DayVote"

    @property
    def target_count(self) -> int:
        return 1

    def is_initiator(self, player: Player) -> bool:
        return player[Property.can_vote]

    def is_target(self, player: Player) -> bool:
        return player[Property.is_exposed]

    def process(self, initiator: Players, target: Optional[Players], registrar: Registrar):
        assert isinstance(initiator, list) or \
               isinstance(initiator, tuple) or \
               isinstance(initiator, set) or \
               isinstance(initiator, int)
        
        if isinstance(initiator, int):
            target.votes = initiator
        else:
            assert all(player[Property.can_vote] for player in initiator)
        
            target.votes += len(initiator)
            for player in initiator:
                player[Property.can_vote] = False
            
        self.register(registrar, initiator, target)
    
    @Action.rollback_preprocess
    def rollback(self, initiator: Union[Players, int], target: Optional[Players], rollback: Rollback):
        assert isinstance(initiator, tuple) or \
               isinstance(initiator, list) or \
               isinstance(initiator, set) or \
               isinstance(initiator, int)
        
        if isinstance(initiator, int):
            target.votes -= initiator
            return
        
        if all(isinstance(pl, Player) for pl in initiator):
            target[Property.can_vote] = True
            target.votes -= len(initiator)
    
    
    def end_action(self, players: Players):
        votes = tuple(player.votes for player in players if player[Property.is_alive] if player.votes != 0)
        
        if sum(votes) != len(players):
            return
        
        for player in players:
            player[Property.can_vote] = False
            
        max_votes = max(votes)
        
        for player in filter(lambda player: player.votes == max_votes, players):
            player[Property.is_victim] = True

    def is_pseudo(self, players: Tuple[Player]) -> bool:
        return not any(player[Property.can_vote] for player in players)
    
    