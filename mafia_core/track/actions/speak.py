from datetime import timedelta
from typing import Optional

from mafia_core.game import GameLogicError
from mafia_core.registrat import Rollback, Registrar
from mafia_core.track.actions import Action
from mafia_core.player import Property, Player
from .action import Players



class Speak(Action):
  
    def get_process_time(self) -> Optional[timedelta]:
        return timedelta(minutes=1)

    def is_initiator(self, player: Player) -> bool:
        return player[Property.is_speak]

    def is_target(self, player: Player) -> bool:
        return not player[Property.is_exposed]

    @property
    def title(self):
        return "Speak"

    @property
    def initiator_count(self) -> int:
        return 1

    @property
    def target_count(self) -> int:
        return 1

    def is_target_optional(self):
        return True

    @Action.preprocess
    def process(self, initiator: Player, target: Players, registrar: Registrar):
        assert initiator[Property.is_speak]
        
        initiator[Property.is_speak] = False
        initiator[Property.right_to_expose] = False
        
        if target is None:
            return
        
        if not initiator[Property.can_expose]:
            raise GameLogicError(f"Player {initiator.number} has no right to expose.")
        
        if target[Property.is_exposed]:
            raise GameLogicError(f"Player {target.number} is already exposed.")
        
        target[Property.is_exposed] = True
    
    @Action.rollback_preprocess
    def rollback(self, initiator: Player, target: Optional[Players], rollback: Rollback):
        initiator[Property.is_speak] = True
        initiator[Property.right_to_expose] = True
        if target:
            target[Property.is_exposed] = False
