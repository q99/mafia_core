from abc import abstractmethod
from typing import Tuple

from .states import State
from mafia_core.player import Role, Player


class Track:
    @classmethod
    @abstractmethod
    def start(cls) -> State:
        pass

    @abstractmethod
    def next(self, state: State, players: Tuple[Player], spent_cards=None) -> State:
        pass

    @abstractmethod
    def player_count(self, role: Role=None)->int:
        pass

    @staticmethod
    @abstractmethod
    def max_count() -> int:
        pass

    @staticmethod
    @abstractmethod
    def min_count() -> int:
        pass

    @abstractmethod
    def is_win(self, players: Tuple[Player]) -> bool:
        return True
