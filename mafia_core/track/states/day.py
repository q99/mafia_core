from typing import Tuple

from .state import State

from mafia_core.card import Card
from mafia_core.player import Player, Property
from mafia_core.track.actions import Action, Speak, DayVote

from mafia_core.track.cards import actions as CardActions

class Day(State):
    def __init__(self, players: Tuple[Player], available_cards: Tuple[Card]):
        if players is None:
            return
        
        super().__init__(players, available_cards)
        
        self._cards = tuple(filter(lambda card: not card.is_night, self._cards))
        
        assert all(player[Property.is_alive] for player in self._players)
        
        self._main_actions = list(Speak() for _ in self._players)
        
        self._players = players
        
        self._additional_actions: Tuple[Action] = \
            tuple(CardActions[card]() for card in self._cards)
    
    @property
    def exposed_count(self) -> int:
        return len(tuple(filter(lambda pl: pl[Property.is_exposed], self._players)))
    
    def __getitem__(self, item: int) -> Action:
        if item >= len(self._main_actions) and not isinstance(self._main_actions[-1], DayVote):
            tuple(self._main_actions.append(DayVote()) for _ in range(self.exposed_count))
        
        return self._main_actions[item]

    def __len__(self):
        return len(self._main_actions) + self.exposed_count
        
    def is_lynching(self, players: Tuple[Player]) -> bool:
        return any(player[Property.is_victim] for player in players)

    def additional_actions(self) -> Tuple[Action]:
        return self._additional_actions

