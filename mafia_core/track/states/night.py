from typing import Optional, Tuple, List, Iterable

from .state import State

from mafia_core.card import Card
from mafia_core.player import Role, Property
from mafia_core.track.actions import Action
from mafia_core.track.cards import actions as CardsActions

class Night(State):
    def __init__(self, players, available_cards: Iterable[Card]):
        super().__init__(None, available_cards)

        self._additional: Tuple[Action] = \
            tuple(CardsActions[card]() for card in self._cards if card.is_night)
        
        from mafia_core.track.actions import NightVote, CommissarReview
        self._main: Tuple[Action] = (NightVote(), CommissarReview())
        
        
    def additional_actions(self) -> Tuple[Action]:
        return self._additional

    def is_lynching(self, players) -> bool:
        return any(player[Property.is_victim] for player in players)

    def __getitem__(self, item: int) -> Action:
        return self._main.__getitem__(item)

    def __len__(self):
        return len(self._main)
