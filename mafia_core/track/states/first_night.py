from typing import Tuple, Set, Union

from mafia_core.track.actions import Action
from mafia_core.track.states import State
import mafia_core.track.actions as actions
from mafia_core.card import Card
from mafia_core.track.cards import actions as CardsActions

class FirstNight(State):
    
    def __init__(self, players, available_cards: Union[Set[Card], Tuple[Card]]):
        super().__init__(players, available_cards)
        
        self._actions: Tuple[Action] = \
            tuple(CardsActions[card]() for card in self._cards if card.is_night)
    
        
        self._chain = [actions.MafiaAcquaintance(),
                actions.NightVote(),
                actions.CommissarAcquaintance(),
                actions.CommissarReview()]
    
    def __getitem__(self, item: int) -> Action:
        return self._chain[item]
    
    def __len__(self):
        return len(self._chain)
    
    def additional_actions(self) -> Tuple[Action]:
        return self._actions

    @property
    def title(self) -> str:
        return "FirstNight"
    