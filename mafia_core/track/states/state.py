from abc import abstractmethod
from typing import Tuple, Optional, Iterable

from mafia_core.track.actions import Action
from mafia_core.track.cards import Card
from mafia_core.player import Role, Player, Property


class State:
    def __init__(self, players: Optional[Tuple[Player]], available_cards: Optional[Iterable[Card]]):

        self._cards = available_cards
        if self._cards:
            self._cards = tuple(self._cards)
        self._current: int = 0
        assert players is None or all(player[Property.is_alive] for player in players)
        self._players: Tuple[Player] = players

    @property
    @abstractmethod
    def title(self) -> str:
        pass

    @abstractmethod
    def __getitem__(self, item: int) -> Action:
        pass

    @abstractmethod
    def __len__(self):
        pass

    @abstractmethod
    def additional_actions(self) -> Tuple[Action]:
        pass

    def is_lynching(self, players: Tuple[Player]) -> bool:
        return any(player[Property.is_victim] for player in players)

    @property
    def is_win(self) -> Optional[Role]:
        return None

    @property
    def action(self) -> Action:
        return self[self._current]

    def next_action(self):
        self._current += 1
        return self.action

    @property
    def is_end(self) -> bool:
        try:
            _ = self[self._current + 1]
            return False
        except IndexError:
            return True

