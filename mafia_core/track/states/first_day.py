from typing import Tuple

from mafia_core.player import Player
from mafia_core.track.actions import Action
from mafia_core.track.actions.wait import Wait
from mafia_core.track.cards import Undercover
from mafia_core.track.states import State


class FirstDay(State):
    @property
    def title(self) -> str:
        return "FirstDay"

    def is_lynching(self, players: Tuple[Player]) -> bool:
        return False

    @property
    def action(self) -> Action:
        return Wait()
    
    def additional_actions(self) -> Tuple[Action]:
        return Undercover(),

    @property
    def is_end(self) -> bool:
        return True
    
    
