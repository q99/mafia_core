from typing import Tuple, Iterable, Type, List

from mafia_core.player import Player, Property
from mafia_core.track.states.state import State

import mafia_core.track.cards as cards
from mafia_core.card import Card
from mafia_core.track.actions import Action, Death

_cards_action = cards.Armor, cards.Treatment

def _get_victims_count(players: Tuple[Player]) -> int:
    return len(tuple(filter(lambda pl: pl[Property.is_victim], players)))

class Lynching(State):
    def __init__(self, next_: Type[State], players: Tuple[Player], available_cards: Iterable[Card]):
        super().__init__(players, available_cards)
    
        self.next: Type[State] = next_
    
    def is_lynching(self, players) -> bool:
        return False

    def additional_actions(self) -> Tuple[Action]:
        if _get_victims_count(self._players) > 1:
            actions = (cards.Leader(),)
        else:
            actions = tuple(action() for action in _cards_action if action.card in self._cards)
        
        return actions

    def __getitem__(self, item):
        if item != 0:
            raise IndexError("`Lynching` state have only one state - `Death`")
        
        return Death()

    def __len__(self):
        return 1

    @property
    def title(self) -> str:
        return "Lynching"
    