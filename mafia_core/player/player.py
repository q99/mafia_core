from dataclasses import dataclass, field
from typing import Dict, Optional, Any, Callable, Tuple, Union
from uuid import UUID

from mafia_core.card import Card
from mafia_core.player import Property
from mafia_core.player import Role


@dataclass
class Player:
    number: int
    channel: Callable[[int, Any], None]
    card: Optional[Card] = None
    role: Optional[Role] = Role.Civilian
    properties: Dict[Property, bool] = None
    votes: int = field(default=0)
    uid: Optional[UUID] = None

    def __post_init__(self):
        if self.properties is None:
            self.properties = {prop: prop.default for prop in Property}

    def __str__(self):
        return f"{self.number} {self.card.title if self.card else ''} {self.role.name} " \
            f"{'; '.join(map(lambda pair: f'{pair[0]}: {pair[1]}', self.properties.items()))} { self.uid}"

    def __hash__(self):
        return hash(self.number)

    def __getitem__(self, item: Property):
        return self.properties[item]

    def __setitem__(self, key: Property, value: bool):
        self.properties[key] = value

    def accept_information(self, info: Any):
        if self.channel:
            self.channel(self.number, info)

    def refresh(self, is_day_end: bool):
        self.votes = 0

        if is_day_end:
            properties = filter(lambda prop: prop.is_day_refresh, tuple(Property))
        else:
            properties = filter(lambda prop: prop.is_night_refresh, tuple(Property))

        for property in properties:
            self.properties[property] = property.default


Players = Union[Player, Tuple[Player]]
