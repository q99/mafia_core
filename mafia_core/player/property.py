from enum import Enum


class Property(Enum):
    can_vote = 0, True, True
    is_exposed = 1, False, True
    can_expose = 2, True, True

    right_to_expose = 3, True, True
    is_speak = 4, True, True

    is_night_protected = 5, False, False
    is_day_protected = 6, False, False

    is_night_review_protected = 7, False, False

    is_alive = 8, True, None
    is_victim = 9, False, None

    @property
    def default(self):
        return self.value[1]

    @property
    def is_day_refresh(self):
        return self.value[2] is True

    @property
    def is_night_refresh(self):
        return self.value[2] is False
