from enum import Enum


class Role(Enum):
    Civilian = 0
    Mafia = 1
    Commissar = 2

    @classmethod
    def default(cls):
        return cls.Civilian
