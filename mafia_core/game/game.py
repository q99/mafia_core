import random
from dataclasses import dataclass
from typing import Iterable, Tuple, Dict, Union, Callable, Optional, Set, overload

from mafia_core.card import Card
from mafia_core.player import Player, Property, Role
from ..track.states import State
from mafia_core.track.track import Track
from mafia_core.track.actions import Action


@dataclass
class Game:
    track: Track
    _players: Union[Tuple[int], Tuple[Player], Dict[int, Player]]
    channel: Callable = None
    uid: Optional[int] = None
    
    def __post_init__(self):
        self._state: State = self.track.start()
        assert len(self._players) == self.track.player_count()

        numbers = list(range(1, len(self._players) + 1))
        random.shuffle(numbers)

        if isinstance(self._players, tuple) or isinstance(self._players, list) or isinstance(self._players, set):
            if all(isinstance(player, int) for player in self._players): # players is collection of member's ids
                self._players = tuple(Player(number=numbers.pop(), uid=uid, channel=self.channel)
                                          for uid in self._players)

            self._players: Dict[int, Player] = \
                {player.number: player for player in self._players}

        self._vote_queue = list()

    def next_exposed(self) -> Player:
        if self._vote_queue:
            return self._vote_queue.pop(0)

        raise IndexError("Can't get next exposed.")

    def end_vote(self):
        assert not self._vote_queue

    def refresh_properties(self):
        raise NotImplementedError()

    def _update_vote_queue(self):
        new_exposed = tuple(filter(lambda player: player not in self._vote_queue, self.exposed))
        if new_exposed:
            assert len(new_exposed) == 1
            self._vote_queue.append(new_exposed[0])

    def current_action(self) -> Action:
        return self._state.action
    
    def get_action(self, action_title: str) -> Action:
        if self.current_action().title == action_title:
            return self.current_action()
        return self.additional_actions(action_title)
    
    @overload
    def additional_actions(self, action_title: str) -> Action:
        pass
    
    @overload
    def additional_actions(self) -> Dict[str, Action]:
        pass
    
    def additional_actions(self, action_title: Optional[str] = None) -> Union[Action, Dict[str, Action]]:
        if action_title is not None:
            for action in self._state.additional_actions():
                if action_title.lower() == action.title.lower():
                    return action
            raise ValueError(f"Can't find in additional actions {action_title}")
        
        return {action.title: action for action in  self._state.additional_actions()
            if action.is_roles_contained(self.current_action()) and not action.is_pseudo(self._players.values())}

    def next_action(self) -> Action:
        self.current_action().end_action(self.alive)

        if self._state.is_end:
            self._state = self.track.next(self._state, self.alive, self.spent_cards)
            return self._state.action

        self._update_vote_queue()
        action = self._state.next_action()
        if action.is_pseudo(self.alive):
            return self.next_action()
        return action

    @overload
    def __getitem__(self, item: int) -> Player:
        pass

    @overload
    def  __getitem__(self, item: Tuple[int]) -> Tuple[Player]:
        pass

    def __getitem__(self, item: Union[int, Tuple[int]]) -> Union[Player, Tuple[Player]]:
        if isinstance(item, tuple):
            assert all(isinstance(num, int) for num in item)
            return self.get_players(*item)

        return self._players[item]

    def get_players(self, *numbers: Tuple[int], filter_: Callable[[Player], bool] = None) -> Tuple[Player]:
        players = map(lambda num: self[num], numbers)
        if filter_:
            players = filter(filter_, players)
        return tuple(players)


    def players_with_close_card(self, roles: Iterable[Role] = None) -> Tuple[Player]:
        players = filter(lambda pl: pl.card is None or pl.card.is_reusable, self.alive)
        if roles is not None:
            players = filter(lambda pl: pl.role in roles, players)

        return tuple(players)

    @property
    def players(self) -> Iterable[Player]:
        players = self._players.values()
        return sorted(players, key=lambda player: player.number)

    def filter_alive(self, filter_: Callable[[Player], bool]) -> Tuple[Player]:
        return tuple(filter(filter_, self.alive))

    @property
    def alive(self) -> Tuple[Player]:
        return tuple(filter(lambda player: player[Property.is_alive], self.players))

    @property
    def mafia(self) -> Tuple[Player]:
        return self.filter_alive(lambda player: player.role is Role.Mafia and
                                                player[Property.is_alive])

    @property
    def commissar(self) -> Optional[Player]:
        for player in self.alive:
            if player.role is Role.Commissar:
                return player

        return None

    @property
    def exposed(self) -> Tuple[Player]:
        return tuple(filter(lambda player: player[Property.is_exposed], self.alive))

    @property
    def victim(self) -> Optional[Player]:
        victim = tuple(filter(lambda player: player[Property.is_victim], self.alive))

        if not victim:
            return None

        assert len(victim) == 1
        return victim[0]

    @property
    def spent_cards(self) -> Set[Card]:
        return set(player.card for player in self.players
                         if player.card is not None and not player.card.is_reusable)


