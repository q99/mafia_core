create table actions
(
	title TEXT not null
		constraint actions_pk
			primary key
);

create unique index actions_title_uindex
	on actions (title);

create table games
(
	uid integer not null
		constraint games_pk
			primary key autoincrement,
	ts DATETIME default CURRENT_TIMESTAMP not null,
	comment TEXT
);

create unique index games_uid_uindex
	on games (uid);

create table history
(
	game int not null
		references games,
	action text not null
		references actions,
	player int not null,
	move_number int not null,
	is_target BOOLEAN not null
);

create table members
(
	uid integer not null
		primary key autoincrement,
	name varchar(255),
	is_master boolean default false
);

create unique index members_name_uindex
	on members (name);

