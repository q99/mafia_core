#!/bin/python3.7

import argparse
from pathlib import Path

from mafia_bot.bot import MafiaBot
from mafia_manager import Storage


def main():
    parser = argparse.ArgumentParser(description="Mafia bot")
    parser.add_argument('--token',
                        help='token of bot',
                        dest='token',
                        type=str)

    parser.add_argument('--database',
                        help='path to database file',
                        dest='database',
                        type=str,
                        default=Path.home() / ".cache" / "mafia.db")

    args = parser.parse_args()
    import logging
    logging.basicConfig(level=logging.DEBUG)
    MafiaBot(args.token, Storage(args.database)).start()

if __name__ == '__main__':
    main()
